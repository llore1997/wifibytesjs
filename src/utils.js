let CACHE_TEMPLATES = new Map();

// From Jake Archibald's Promises and Back:
// http://www.html5rocks.com/en/tutorials/es6/promises/#toc-promisifying-xmlhttprequest

/**
 * @function
 * @name get -The get function loads data from the server using a HTTP GET request by default.
 */


function get(url, method='GET' ,...data) {
    // Return a new promise.
    return new Promise(function(resolve, reject) {
      if (CACHE_TEMPLATES.has(url)) {
        resolve(CACHE_TEMPLATES.get(url));
      }else{
        // Do the usual XHR stuff
        var req = new XMLHttpRequest();
        req.open(method, url);
    
        
        req.onload = function() {
          // This is called even on 404 etc
          // so check the status
          if (req.status == 200) {
            // Resolve the promise with the response text
            CACHE_TEMPLATES.set(url,req.response);
            resolve(req.response);
          }
          else {
            // Otherwise reject with the status text
            // which will hopefully be a meaningful error
            reject(Error(req.statusText));
          }
        };
    
        // Handle network errors
        req.onerror = function() {
          reject(Error("Network Error"));
        };
    

       
       
      }

      if (data.length <=0){
        console.log("No hi han dades");
        console.log(data)
        req.send();

      }else{
        console.log("Hi han dades")
        let json =JSON.stringify(data);
        console.log(json);
      
         req.send(json);
      }
      
    });
  }
  

  export {get};