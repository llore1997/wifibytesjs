import {get} from '../utils';
import {Settings} from '../settings';
import {templateavislegal} from '../templates/avislegal.html';



/**
 * The function of the AvislegalCtrl is to take the information from the server, 
 * through several endpoints, such as /dataBusiness, 
 * after obtaining the information,
 * it injects and shows the information through a literal template.
 *  @param {object} avislegal - Information obtained from the endpoint
 */


class AvislegalControler {

    constructor() {
        
    }

     /**render*/
    static render() {

            let datos_empresaavislegal = get(Settings.baseURL+'/datos_empresa').then(function(response) {           
            document.getElementById('main2').innerHTML="";
            document.getElementById('jumbotron').innerHTML="";

                let avislegalREG = new RegExp("Avis Legal");
                let datosEmpresaavislegal=JSON.parse(response).textos;
                
                     
            let avislegal =  datosEmpresaavislegal.filter((data) =>{

                        if ( avislegalREG.test(data.key) ) {
                            console.log(data.content);
                            return data.content

                        } 
                    })
                try{document.getElementById('main').innerHTML =templateavislegal(avislegal);}catch(e){console.log("error")};
            }).catch(function(error) {
            console.log("Failed!", error);
          }); 
      }
  }


export default AvislegalControler;
