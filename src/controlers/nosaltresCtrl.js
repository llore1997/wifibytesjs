import {get} from '../utils';
import {Settings} from '../settings';
import {templatenosaltres} from '../templates/nosaltres.html';



/**
 * The function of the nosaltresCtrl is to take the information from the server, 
 * through several endpoints, such as /dataBusiness, 
 * after obtaining the information,
 * it injects and shows the information through a literal template.
 * @param {object} nosaltres - Information obtained from the endpoint
 */


class NosaltresControler {

    constructor() {
        
    }

     /**render*/
    static render() {

        let datos_empresacar = get(Settings.baseURL+'/datos_empresa').then(function(response) {           
        document.getElementById('main2').innerHTML="";
        document.getElementById('jumbotron').innerHTML="";

            let nosaltresREG = new RegExp("Nosaltres");
            let datosEmpresanosaltres=JSON.parse(response).textos;
            
            
       let nosaltres =  datosEmpresanosaltres.filter((data) =>{

                if ( nosaltresREG.test(data.key) ) {
                    return data.content

                } 
            })
        
    
            try{document.getElementById('main').innerHTML =templatenosaltres(nosaltres);}catch(e){console.log("error")};
          }).catch(function(error) {
            console.log("Failed!", error);
          }); 
      }
  }


export default NosaltresControler;
