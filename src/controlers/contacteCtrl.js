import {get} from '../utils';
import {Settings} from '../settings';
import {template} from '../templates/contacte.html';



/**
 * The function of the contactCtrl is to take the information from the server,
 * through several endpoints, such as / dataBusiness, 
 * after obtaining the information,
 * it injects and shows the information through a literal template.
 */


class ContacteControler {

    constructor() {
        
    }
  
    /** render  */
    static render() {
        /**/
 
     
        let datos_empresacontacte = get(Settings.baseURL+'/datos_empresa').then(function(response) {           
            let datosEmpresacontacte=JSON.parse(response);
            try{
              document.getElementById('main').innerHTML =template(datosEmpresacontacte);
              document.getElementById('main2').innerHTML="";
              initMap();


  
  
    document.getElementById("button").addEventListener("click",contact)
              }catch(e){
                console.log("error")};
              
            }).catch(function(error) {
              console.log("Failed!", error);
            });
      }
  }


  /** 
   * @function 
   * @name contact -The function of the contact serves to collect and validate, the information that the user enters in the contact form,
  */
  function contact(){

    let emailPattern = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
    let phonenumber =new RegExp('[0-9]{9}')
    let errorname=document.getElementById("errorname").innerHTML=""
    let erroremail=document.getElementById("erroremail").innerHTML=""
    let errortextarea=document.getElementById("errortextarea").innerHTML=""
    let errorphone=document.getElementById("errorphone").innerHTML=""

    let name=document.getElementById("name").value
    let email=document.getElementById("email").value
    let textarea=document.getElementById("textarea").value
    let phone=document.getElementById("phone").value

       let data = { 
         name:name, 
         phone:phone,
         email:email,
         textarea:textarea
        }


        if (data.name.length < 4 || data.name.length > 15 ){
          document.getElementById("errorname").innerHTML="The entered name is not valid"
      
        }else if (data.phone.length < 1 || !phonenumber.test(data.phone)){
            document.getElementById("errorphone").innerHTML=""

         }else  if (data.email.length < 1 || !emailPattern.test(data.email)){
                  document.getElementById("erroremail").innerHTML="The email entered is not valid, check it"

                  } else if (data.textarea.length < 7 || data.textarea.length > 150 ){
                    document.getElementById("errortextarea").innerHTML="The description entered is not valid, it must contain from 6 to 150 words"

                }
           // get(Settings.baseURL+ "/contacto/","POST",data).then(function(response) {
  };
  
        /** 
         * @function
         * @name initMap -It is the function of the init map used to collect the information of the map and the location of where it should be shown that information in the view
        */
        function initMap() {
          let map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 38.765324 , lng: -0.6121269999999868},
            zoom: 13,
          });
          var marker = new google.maps.Marker({
            position: {lat: 38.765324, lng: -0.6121269999999868},
            map: map,
      title: 'Bocairent'
          });
        }


export default ContacteControler;
