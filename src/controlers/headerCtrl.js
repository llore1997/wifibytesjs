import {get} from '../utils';
import {Settings} from '../settings';
import {templateheader} from '../templates/header.html';



/**
 * The function of the headerCtrl is to take the information from the server, 
 * through several endpoints, such as /dataBusiness, 
 * after obtaining the information,
 * it injects and shows the information through a literal template.
 *  @param {object} datosEmpresaheader - Information obtained from the endpoint
 */


class HeaderControler {

    constructor() {
        
    }

     /**render*/
    static render() {

     let datos_empresaheader = get(Settings.baseURL+'/datos_empresa').then(function(response) {   
        let datosEmpresaheader=JSON.parse(response);
          try{document.getElementById('nav').innerHTML =templateheader(datosEmpresaheader);}catch(e){console.log("error")};
            }).catch(function(error) {
                console.log("Failed!", error);
              });
     };

 };


export default HeaderControler;
