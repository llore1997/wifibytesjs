import {get} from '../utils';
import {Settings} from '../settings';
import {templatecookies} from '../templates/cookies.html';



/**
 * The function of the cookiesCtrl is to take the information from the server, 
 * through several endpoints, such as /dataBusiness, 
 * after obtaining the information,
 * it injects and shows the information through a literal template.
 * @param {object} cookies - Information obtained from the endpoint
 */


class CookiesControler {

    constructor() {
        
    }

     /**render*/
    static render() {

        let datos_empresacar = get(Settings.baseURL+'/datos_empresa').then(function(response) {           
        document.getElementById('main2').innerHTML="";
        document.getElementById('jumbotron').innerHTML="";

            let cookiesREG = new RegExp("Cookies");
            let datosEmpresacook=JSON.parse(response).textos;
            
            
            
        let cookies =  datosEmpresacook.filter((data) =>{

                    if ( cookiesREG.test(data.key) ) {
                        return data.content

                    } 
                })
    
            try{document.getElementById('main').innerHTML =templatecookies(cookies);}catch(e){console.log("error")};
          }).catch(function(error) {
            console.log("Failed!", error);
          }); 
      }
  }


export default CookiesControler;
