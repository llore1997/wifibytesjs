import {get} from '../utils';
import {Settings} from '../settings';
import {template} from '../templates/footer.html';

/**
 * The function of the footerCtrl is to take the information from the server, 
 * through several endpoints, such as / dataBusiness, 
 * after obtaining the information,
 * it injects and shows the information through a literal template.
 * @param {object} datosEmpresafooterfacetwitt - Information obtained from the endpoint
 * @param {object} footer - Information obtained from the endpoint
 */





class FooterControler {

    constructor() {
        
    }
  
    /** render  */
    static render() {
        
        let datos_empresafooter = get(Settings.baseURL+'/datos_empresa').then(function(response) {   
             let datosEmpresafooterfacetwitt=JSON.parse(response);


            let footerREG = new RegExp("joumbutron");
            let datosEmpresafooter=JSON.parse(response).textos;
            
            
            
        let footer =  datosEmpresafooter.filter((data) =>{

                    if (!footerREG.test(data.key) ) {
                        return data.content

                    } 
                })

            try{document.querySelector('footer').innerHTML =template(footer,datosEmpresafooterfacetwitt);}catch(e){console.log("error")};

          }).catch(function(error) {
            console.log("Failed!", error);
          });


        
        
    }
}
export default FooterControler;
