import {get} from '../utils';
import {Settings} from '../settings';
import {template} from '../templates/home.html';
import {templatetarifa} from '../templates/tarifa.html';
import {templatecar} from '../templates/carousel.html';
/**
 * The function of the HomeCtroler is to take the information from the server, 
 * through several endpoints, such as /home and /tarifa,/datos_empresa  
 * after obtaining the information,
 * it injects and shows the information through a literal template.
 * @param {object} home - Information obtained from the endpoint
 * @param {object} tarifa - Information obtained from the endpoint
 * @param {object} carousel - Information obtained from the endpoint
 */


class HomeControler {

    constructor() {
        
    }
  
    /** render  */
    static render() {
        
        let datos_home = get(Settings.baseURL+'/home').then(function(response) {           
            let home = JSON.parse(response)
            try{document.getElementById('main2').innerHTML =template(home);}catch(e){console.log("error")};
           

          }).catch(function(error) {
            console.log("Failed!", error);
          });



          let datos_tarifa = get(Settings.baseURL+'/tarifa/').then(function(response) {           
             let tarifa = JSON.parse(response).results
             try{document.getElementById('main').innerHTML =templatetarifa(tarifa);}catch(e){console.log("error")};
           

          }).catch(function(error) {
            console.log("Failed!", error);
          });



          let datos_empresacar = get(Settings.baseURL+'/datos_empresa').then(function(response) {           
        
            let carouselREG = new RegExp("joumbutron");
            let datosEmpresacar=JSON.parse(response).textos;
            
            
            let carousel =  datosEmpresacar.filter((data) =>{

                            if ( carouselREG.test(data.key) ) {
                                return data.content

                            } 
                    });
    
           try{document.getElementById('jumbotron').innerHTML =templatecar(carousel);}catch(e){console.log("error")};
         }).catch(function(error) {
          console.log("Failed!", error);
         });


    }
}
export default HomeControler;
